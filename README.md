# Telkom Study Case Chapter 1

Nethaneel Patricio Linggar
Farrel Emerson

### Models
#### Product

```go
package models

type Campaign struct {
	Id           int64  `gorm:"primaryKey" json:"id"`
	NamaCampaign string `gorm:"type:varchar(300)" json:"nama_campaign"`
	ClickThrough int64  `gorm:"type:int" json:"click_through"`
	Conversion   int64  `gorm:"type:int" json:"conversion"`
	NilaiAkhir   int64  `gorm:"type:int" json:"nilai_akhir"`
}
```

Create campaign struct to store json parse

#### Setup

```go
package models

import (
	"gorm.io/driver/mysql"
	"gorm.io/gorm"
)

var DB *gorm.DB

func ConnectDatabase() {
	database, err := gorm.Open(mysql.Open("root:@tcp(localhost:3306)/go_restapi_gin"))
	if err != nil {
		panic(err)
	}

	database.AutoMigrate(&Campaign{})

	DB = database
}
```
Connect golang to mysql using gorm library

### Controller

#### index function
```go
func Index(c *gin.Context) {

	var campaigns []models.Campaign

	models.DB.Find(&campaigns)
	c.JSON(http.StatusOK, gin.H{"campaigns": campaigns})

}
```

#### Show function

```go
func Show(c *gin.Context) {
	var campaign models.Campaign
	id := c.Param("id")

	if err := models.DB.First(&campaign, id).Error; err != nil {
		switch err {
		case gorm.ErrRecordNotFound:
			c.AbortWithStatusJSON(http.StatusNotFound, gin.H{"message": "Data tidak ditemukan"})
			return
		default:
			c.AbortWithStatusJSON(http.StatusInternalServerError, gin.H{"message": err.Error()})
			return
		}
	}

	c.JSON(http.StatusOK, gin.H{"Campaign": campaign})
}
```

#### MaxClickThrough
```go
func MaxClickThrough(c *gin.Context) {
	var campaign models.Campaign

	if err := models.DB.Order("click_through DESC").First(&campaign).Error; err != nil {
		switch err {
		case gorm.ErrRecordNotFound:
			c.AbortWithStatusJSON(http.StatusNotFound, gin.H{"message": "Data tidak ditemukan"})
			return
		default:
			c.AbortWithStatusJSON(http.StatusInternalServerError, gin.H{"message": err.Error()})
			return
		}
	}

	c.JSON(http.StatusOK, gin.H{"Campaign": campaign})
}
```

Basically we sort the data by "click_through" column from the highest to the lowest value and we only take the top of the list.

#### MaxConversion
```go
func MaxConversion(c *gin.Context) {
	var campaign models.Campaign

	if err := models.DB.Order("conversion DESC").First(&campaign).Error; err != nil {
		switch err {
		case gorm.ErrRecordNotFound:
			c.AbortWithStatusJSON(http.StatusNotFound, gin.H{"message": "Data tidak ditemukan"})
			return
		default:
			c.AbortWithStatusJSON(http.StatusInternalServerError, gin.H{"message": err.Error()})
			return
		}
	}

	c.JSON(http.StatusOK, gin.H{"Campaign": campaign})
}
```
This function has the same logic as the MaxClickThrough function, it sort the conversion column from the highest to the lowest value and only takes the first value of the list

#### MaxNilaiAkhir
```go
func MaxNilaiAkhir(c *gin.Context) {
	var campaign models.Campaign

	if err := models.DB.Order("nilai_akhir DESC").First(&campaign).Error; err != nil {
		switch err {
		case gorm.ErrRecordNotFound:
			c.AbortWithStatusJSON(http.StatusNotFound, gin.H{"message": "Data tidak ditemukan"})
			return
		default:
			c.AbortWithStatusJSON(http.StatusInternalServerError, gin.H{"message": err.Error()})
			return
		}
	}

	c.JSON(http.StatusOK, gin.H{"Campaign": campaign})
}
```
Again this function has the same logic as other max value function and only takes the highest value of the descending order

####